// function for googleMap
function myMap() {
    var myCenter = new google.maps.LatLng(52.09450, 23.68666);
    var mapProp = {center:myCenter, zoom:15, scrollwheel:true, draggable:true};
    var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
    var marker = new google.maps.Marker({position:myCenter});
    marker.setMap(map);
}

// function for VKwidget
function VK_Widget_Init(){
    document.getElementById('vk_widget').innerHTML = '<div id="vk_groups"></div>';
    VK.Widgets.Group("vk_groups", {mode: 0, no_cover: 1, width: "auto", height: "312"}, 152481686);
};
window.addEventListener('load', VK_Widget_Init, false);
window.addEventListener('resize', VK_Widget_Init, false);

// function for affix
$(document).ready(function() {
    var stickyToggle = function(sticky, stickyWrapper, scrollElement) {
        if (window.matchMedia('(min-width: 992px)').matches) {
            var stickyHeight = sticky.outerHeight();
            var stickyTop = stickyWrapper.offset().top;
            if (scrollElement.scrollTop() >= stickyTop){
            stickyWrapper.height(stickyHeight);
            sticky.addClass("bg-light");
            sticky.addClass("is-sticky");
            }
            else{
            sticky.removeClass("bg-light");
            sticky.removeClass("is-sticky");
            stickyWrapper.height('auto');
            }
        } else {
            sticky.removeClass("bg-light");
            sticky.removeClass("is-sticky");
            stickyWrapper.height('auto');
        }
    };
    
    // Find all data-toggle="sticky-onscroll" elements
    $('[data-toggle="sticky-onscroll"]').each(function() {
        var sticky = $(this);
        var stickyWrapper = $('<div>').addClass('sticky-wrapper'); // insert hidden element to maintain actual top offset on page
        sticky.before(stickyWrapper);
        sticky.addClass('sticky');
        
        // Scroll & resize events
        $(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function() {
            stickyToggle(sticky, stickyWrapper, $(this));
        });
        
        // On page load
        stickyToggle(sticky, stickyWrapper, $(window));
    });
});