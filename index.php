<?php get_header(); ?>
<div class="container">
	<hr>
	<a href="<?php echo (site_url().'/novosti') ?>" class="btn btn-primary" role="button" aria-pressed="true">НАЗАД К НОВОСТЯМ</a>
    <div class="row">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; else: ?>
            <p><?php _e('Sorry, this page does not exist.'); ?></p>
        <?php endif; ?>
	</div>
	<a href="<?php echo (site_url().'/novosti') ?>" class="btn btn-primary" role="button" aria-pressed="true">НАЗАД К НОВОСТЯМ</a>
</div>
<?php get_footer(); ?>