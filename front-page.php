<?php get_header(); ?>

<!-- carousel -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="15000">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="first-slide" src="<?php bloginfo('stylesheet_directory'); ?>/img/english_course.jpg" alt="First slide">
            <div class="container">
              <div class="carousel-caption">
                <!-- <h1>Курсы английского языка</h1> -->
                <p><b>Курсы английского языка «OUTLOOK»</b> - это отличная возможность для взрослых и детей улучшить свой английский или начать говорить с нуля. Это интересные, эффективные и познавательные занятия, которые помогут вам свободно общаться на английском и открыть новые перспективы.</p>
                <p><a class="btn btn-lg btn-primary" href="<?php echo (site_url().'/kursy_anghliiskogho') ?>" role="button"><b>ПРОГРАММЫ И ЦЕНЫ</b></a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img class="second-slide" src="<?php bloginfo('stylesheet_directory'); ?>/img/china_course.jpg" alt="Second slide">
            <div class="container">
              <div class="carousel-caption">
                <!-- <h1>Курсы китайского языка</h1> -->
                <p><b>Курсы китайского языка «OUTLOOK»</b> - это уникальная возможность овладеть китайским языком на высоком уровне. Мы предлагаем широкий выбор программ, которые подойдут каждому. Наш преподаватель является носителем китайского языка.</p>
                <p><a class="btn btn-lg btn-primary" href="<?php echo (site_url().'/kursy_kitaiskogho') ?>" role="button">ПРОГРАММЫ И ЦЕНЫ</a></p>
              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"><i class="ed-arrow-prev"></i></span>
          <span class="sr-only">Предыдущий</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"><i class="ed-arrow-next"></i></span>
          <span class="sr-only">Следующий/span>
        </a>
      </div>  
      
      <!-- page content -->
      <div class="container">
        <div id="onas" class="row">
          <div class="col-lg-6 teachers">
            <h1>О НАС</h1>
            <p>Курсы "OUTLOOK" направлены на обучение современному иностранному языку в доступной и увлекательной форме. Мы ориентированы на достижение высоких результатов каждого учащегося. Наши преподаватели помогут овладеть навыками общения, расширить словарный запас и активно употреблять его в речи, изучить грамматику как на базовом уровне, так и на углубленном, для сдачи ЦТ и международных экзаменов.</p>
            <p>Мы придерживаемся основного правила передовых западных школ: послушай и прочитай  -  пойми и заговори. На наших занятиях используются учебники зарубежных изданий <b>(Oxford, Cambridge, Longman, Pearson, Macmillan, BLCUP)</b>, которые зарекомендовали себя во всем мире. Разноплановые занятия с использованием аудио- и видеоматериалов, интернет-ресурсов, а также регулярно проводимые семинары с носителями языка помогут вам окунуться в атмосферу иноязычной обстановки. Наша цель: заинтересовать - показать - научить использовать самому!</p>
            <h3 id="prepodavateli">Наши преподаватели:</h3>
            <p><img class="img-responsive ed-img-teachers" src="<?php bloginfo('stylesheet_directory'); ?>/img/1.png" alt="teacher"><b>Мочуговская Наталья Викторовна ­- куратор, преподаватель английского языка.­</b> Учитель английского языка первой категории. В 2003 году окончила Брестский государственный университет им. А.С.Пушкина, факультет иностранных языков по специальности «английский язык, немецкий язык, преподаватель». С 2002 года по 2017 год – учитель английского языка в ГУО «Гимназия №1 г.Бреста».</p>
            <p><b>Подробнее >>></b></p>
            <p><img class="img-responsive ed-img-teachers" src="<?php bloginfo('stylesheet_directory'); ?>/img/2.png" alt="teacher"><b>Ли Пэйнчжен - преподаватель китайского языка.</b> В 2015 году работал преподавателем китайского языка в ГУО «Учебно-педагогический комплекс детский сад-начальная школа №1 г. Бреста». В 2014 году принимал участие в межвузовской олимпиаде по РКИ (руский язык как иностранный). По итогам занял 2-е место. </p>
            <p><b>Подробнее >>></b></p>
          </div>
          <div class="col-lg-6">
            <!-- reviews -->
            <div id="myReviews" class="carousel" data-ride="carousel" data-interval="0">
              <ol class="carousel-indicators">
                <li data-target="#myReviews" data-slide-to="0" class="active"></li>
                <li data-target="#myReviews" data-slide-to="1"></li>
                <li data-target="#myReviews" data-slide-to="2"></li>
                <li data-target="#myReviews" data-slide-to="3"></li>
                <li data-target="#myReviews" data-slide-to="4"></li>
                <li data-target="#myReviews" data-slide-to="5"></li>
                <li data-target="#myReviews" data-slide-to="6"></li>
              </ol>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/img/r1.jpg">
                  <div class="container">
                    <div class="carousel-caption">
                        <h4>Диана</h4><h5>Курсы английского языка</h5>
                        <p>Мне повезло. Я много лет изучала английский язык с Натальей Викторовной. Как до, так и после обучения с ней я пробовала общаться и с другими преподавателями, но никто из них не мог бы сравниться с ней ни в знаниях, ни в методах преподавания. Особенно импонировала мне подобранная ею литература, в особенности иностранные издания (Кембриджского, Оксфордского университетов и др.), а также видео и аудио материалы. Наталья Викторовна - очень отзывчивый и чуткий преподаватель, однако и очень строгий (любого ленивца побудит к обучению). Она никогда не останавливается на достигнутом и постоянно совершенствует свои знания. Для меня английский язык навсегда будет связан лишь с её именем. Высокие результаты и уверенность в своих знаниях вам обеспечены. Обучаясь у неё вы получите не только отличные знания грамматики, богатый словарный запас и произношение, но и самое важное - научитесь чувствовать язык.</p>
                    </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/img/r2.jpg">
                  <div class="container">
                    <div class="carousel-caption">
                      <h4>Екатерина</h4><h5>Курсы английского языка</h5>
                      <p>Добрый день. Я студентка факультета международных бизнес-коммуникаций БГЭУ. Наталья Викторовна- замечательный учитель, отзывчивая, очень умная и начитанная, прямо мастер своего дела. Она работает на знания и результат. К каждому ученику был найден свой подход, поэтому знаниями обладали все. Интересная программа обучения. Я набрала 87 баллов на ЦТ. В общем, с учителем очень повезло, всем советую,не пожалеете!!!</p>
                    </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/img/r3.jpg">
                  <div class="container">
                    <div class="carousel-caption">
                      <h4>Анастасия</h4><h5>Курсы английского языка</h5>
                      <p>Наталья Викторовна - мой любимый педагог. Она всегда проводила занятия так, что мы не только повышали уровень английского языка, но и расширяли наш кругозор. При поступлении в университ на факультет иностранных языков я набрала 85 баллов. Учиться у неё интересно, она со всеми умеет находить общий язык. Наталья Викторовна может преподнести интересно даже сухую грамматику... Мне очень повезло.</p>
                    </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/img/r4.jpg">
                  <div class="container">
                    <div class="carousel-caption">
                      <h4>Лера</h4><h5>Курсы английского языка</h5>
                      <p>Студентка Минского Государственного Лингвистического Университета. Приношу большую благодарность Наталье Викторовне за полученные знания и прекрасную подготовку к Централизованному Тестированию. Очень требовательный и справедливый преподаватель, который вкладывается в каждого ученика и находит особый подход. С полной уверенностью могу рекомендовать Наталью Викторовну и уверена, что люди получат отличные знания и результат оправдает ожидания!</p>
                    </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/img/r5.jpg">
                  <div class="container">
                    <div class="carousel-caption">
                      <h4>Аня</h4><h5>Курсы английского языка</h5>
                      <p>В 11 классе, когда подготовка к ЦТ была в самом разгаре, я по счастливой случайности попала к Наталье Викторовне. Хочу выразить своё исключительно положительное мнение о её методике преподавания, особенно хочется отметить высокий профессионализм, внимательность, ответственность, доброжелательное и терпеливое отношение к ученикам, креативный подход к занятиям и умение заинтересовать. Спасибо Вам!</p>
                    </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/img/r6.jpg">
                  <div class="container">
                    <div class="carousel-caption">
                      <h4>Марк</h4><h5>Курсы английского языка</h5>
                      <p>Наталья Викторовна - прекрасный преподаватель. Она чётко и ясно объясняет, на какие моменты стоит обратить внимание в заданиях того или иного типа. К концу занятий многие задания научился решать почти на автомате. Также она даёт глубокие знания по различным разделам, на которые идут задания в ЦТ. Если что-то непонятно, очень хорошо объясняет. Перед ЦТ чётко расписывает, что надо повторить, поэтому у вас не будет сомнений, на что делать упор. Так что всем рекомендую, можете быть уверены, что после занятий у неё вы сдадите ЦТ более чем хорошо.</p>
                    </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/img/r7.jpg">
                  <div class="container">
                    <div class="carousel-caption">
                      <h4>Марина</h4><h5>Курсы английского языка</h5>
                      <p>Так получилось, что за время учёбы в гимназии часто менялись учителя по английскому. В 10 классе, я начала заниматься с Натальей Викторовной. За 2 года с Натальей Викторовной выучилось и стало понятным почему-то намного больше вещей, чем до этого. Единственное что.. Даже если ты был не в настроении и хотел таким же оставаться, тебя все равно заставляли улыбаться и быть активным. Поэтому сидеть и грустить о чем-то своём не выйдет, но зато поразмышлять и помечтать, естественно, на английском на все 100%! Наталье Викторовне огромнейшее спасибо за всё :)</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>  
          </div>
        </div>
        <div id="novosti" class="row">
          <div class="col-lg-6">
            <h1 style="margin-bottom:24px">НОВОСТИ</h1>
            <?php 
            // the query
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>3)); ?>
            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <p><?php echo get_post_meta( $post->ID, 'description', true ); ?></p>
                    <p><?php the_date('Y-m-d'); ?></p>
                    <hr>
                <?php endwhile; ?>
                <!-- end of the loop -->
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
          </div>
          <div class="col-lg-6">
            <div id="instagram-widget" class="container">
            <p class=ed-inst-header>Мы в Instagram <img class="ed-img-inst" src="<?php bloginfo('stylesheet_directory'); ?>/img/inst.svg" alt="instagramm"></p>
              <!-- SnapWidget for Instagram -->
              <iframe src="https://snapwidget.com/embed/499848" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no"></iframe>
            </div>
            <!-- VK Widget -->
            <div id="vk_widget"></div>
          </div>
        </div>
        <div id="kontakti" class="row">
          <div class="col-lg-12">
            <h1>КОНТАКТЫ</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4">
              <table class="table table-condensed ed-table-contacts">
                <tr>
                  <td>Телефон</td>
                  <td><a href="tel:+375(29)7231312">+375 (29) 723 13 12</a></td>
                </tr>
                <tr>
                  <td>Адрес</td>
                  <td>Беларусь, г. Брест, ул. Пушкинская, дом 6, 17</td>
                </tr>
                <tr>
                  <td>Skype</td>
                  <td><a href="skype:kursybrest?call">kursybrest</a></td>
                </tr>
                <tr>
                  <td>E-mail</td>
                  <td><a href="mailto:kontakt@kursybrest.by">kontakt@kursybrest.by</a></td>
                </tr>
              </table>
              <!-- Add Google Maps -->
              <div id="googleMap" class="container"></div>
          </div>
          <div class="clearfix"></div>
          <div class="col-md-6 col-lg-4">
            <h3>Форма обратной связи</h3>
            <?php echo do_shortcode( '[contact-form-7 id="72" title="Contact Form"]' ); ?>
          </div>
          <div class="col-md-6 col-lg-4">
            <h3>Наши партнеры</h3>
            <p class="ed-p-partners"><a href="http://brl.by/" target="_blank"><img class="img-responsive ed-img-partners" src="<?php bloginfo('stylesheet_directory'); ?>/img/brl.png" alt="blr"></a>Брестская областная библиотека имени М. Горького</p>
            <p class="ed-p-partners"><a href="https://vk.com/lexicon_shop" target="_blank"><img class="img-responsive ed-img-partners" src="<?php bloginfo('stylesheet_directory'); ?>/img/lexicon.jpg" alt=""></a>Магазин иностранной литературы "ЛЕКСИКОН"</p>
          </div>
        </div>
      </div>

<?php get_footer(); ?>