<?php get_header(); ?>

<div class="container">
  <hr>
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <?php the_content(); ?>
  <?php endwhile; else: ?>
      <p><?php _e('Sorry, this page does not exist.'); ?></p>
  <?php endif; ?>

  <!-- The Modal -->
  <div class="modal fade" id="ed-callback">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Заказать звонок с сайта</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <?php echo do_shortcode( '[contact-form-7 id="86" title="CallBack"]' ); ?>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Назад</button>
        </div>
        
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>