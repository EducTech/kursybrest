<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <hr>
            <h1 style="margin-bottom:24px">НОВОСТИ</h1>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                <p><?php echo get_post_meta( $post->ID, 'description', true ); ?></p>
                <p><?php the_date('Y-m-d'); ?></p>
                <hr>
            <?php endwhile; else: ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>