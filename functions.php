<?php 
// Register Script
function enqueue_custom_script()
{
	// Register the script like this for a theme:
	wp_register_script( 'ed_courses_script', get_template_directory_uri() . '/js/main.js', '1.0', false );
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'ed_courses_script' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_custom_script' );
?>