<!doctype html>
<html lang="en">
  <head>
    <title><?php wp_title('|',1,'right'); ?> <?php bloginfo('description'); ?> <?php bloginfo('name'); ?></title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300i,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700,300i" rel="stylesheet">

    <!-- script -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> <!-- JQUERY -->
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?151"></script> <!-- vk api -->
    <script src="https://snapwidget.com/js/snapwidget.js"></script> <!-- instagram widget -->
    <?php wp_enqueue_script("custom-script"); ?>

    <!-- Custom CSS -->
    <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">

    <?php wp_head(); ?>
  </head>
  
  <body>
    <!-- header -->
    <header>
      <div class="container">
        <div class="row">
          <div class="header-content-left col-lg-5">
            <div class="ul-widget-icon">
              <a href="<?php echo site_url(); ?>">
                <img id="outlook-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/logo.png" class="ul-w-icon-size-72" alt="Outlook">
              </a>
            </div>
            <div class="content-left">
              <a href="<?php echo site_url(); ?>"><h1>OUTLOOK</h1></a>
              <p id="slogan">студия английского и китайского языков</p>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="header-content-right">
                <div class="ed-header-contacts">
                  <ul>
                    <li class="ed-li-contacts-adress">Беларусь, г. Брест, ул. Пушкинская, дом 6, 17</li>
                    <li class="ed-li-contacts-skype"><a href="skype:kursybrest?call">kursybrest</a></li>
                    <li class="ed-li-contacts-tel"><a href="tel:+375(29)7231312">+375 (29) 723 13 12</a></li>
                    <li class="ed-li-contacts-mail"><a href="mailto:kontakt@kursybrest.by">kontakt@kursybrest.by</a></li>
                  </ul>
                </div>
                <div class="ed-header-social">
                  <a href="https://vk.com/kursybrest" class="ed-a-social ed-social-vk" target="_blank"></a>
                  <a href="https://instagram.com/kursybrest/" class="ed-a-social ed-social-inst" target="_blank"></a>
                </div>
            </div>

            <!-- navbar -->
            <nav class="navbar navbar-expand-lg navbar-light ed-sticky-ease" data-toggle="sticky-onscroll">
              <button class="navbar-toggler ed-navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon ed-navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto text-center">
                  <li class="nav-item ed-nav-item">
                    <a class="nav-link ed-nav-link ed-sticky-ease" href="<?php echo (site_url().'/kursy_anghliiskogho') ?>">КУРСЫ АНГЛИЙСКОГО<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item ed-nav-item">
                    <a class="nav-link ed-nav-link ed-sticky-ease" href="<?php echo (site_url().'/kursy_kitaiskogho') ?>">КУРСЫ КИТАЙСКОГО</a>
                  </li>
                  <li class="nav-item ed-nav-item dropdown">
                    <a class="nav-link ed-nav-link dropdown-toggle ed-sticky-ease" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      ИНФОРМАЦИЯ И ОТЗЫВЫ
                    </a>
                    <div class="dropdown-menu text-center" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="<?php echo wp_make_link_relative(site_url().'#onas'); ?>">О НАС</a>
                      <a class="dropdown-item" href="<?php echo wp_make_link_relative(site_url().'#prepodavateli'); ?>">НАШИ ПРЕПОДАВАТЕЛИ</a>
                      <a class="dropdown-item" href="<?php echo wp_make_link_relative(site_url().'#kontakti'); ?>">КОНТАКТЫ</a>
                      <a class="dropdown-item" href="<?php echo (site_url().'/novosti') ?>">НОВОСТИ</a>
                    </div>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </header>